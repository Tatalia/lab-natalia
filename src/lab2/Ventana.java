/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

import datos.Logic;

/**
 *
 * @author nati2
 */
public class Ventana extends javax.swing.JFrame {

    /**
     * Creates new form Ventana
     */
    Logic logic = new Logic();
    int[] updatedValues1 = new int[6];   
    int[] updatedValues2 = new int[6];
    
    boolean playPlayer1 = true;
    boolean playPlayer2 = true;
    boolean startAgain = false;
    
    public Ventana() {
        initComponents();
        rbTurno1Player1.doClick();
        barJugador1.setValue(0);
        updatedValues1[1] = 1;
        updatedValues1[2] = 1;
        updatedValues1[3] = 0;
        updatedValues1[4] = 0;
        updatedValues1[5] = 0;
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        playerOneGroup = new javax.swing.ButtonGroup();
        playerTwoGroup = new javax.swing.ButtonGroup();
        pnlPlayer2 = new javax.swing.JPanel();
        rbTurno3Player2 = new javax.swing.JRadioButton();
        rbTurno1Player2 = new javax.swing.JRadioButton();
        rbTurno2Player2 = new javax.swing.JRadioButton();
        barJugador2 = new javax.swing.JProgressBar();
        lblBalon1J2 = new javax.swing.JLabel();
        lblBalon2J2 = new javax.swing.JLabel();
        lblBalon3J2 = new javax.swing.JLabel();
        lblBalon4J2 = new javax.swing.JLabel();
        lblBalon5J2 = new javax.swing.JLabel();
        btnLanzarJugador2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblResultdadoLanzarJ2 = new javax.swing.JLabel();
        lblTotalPontsJ2 = new javax.swing.JLabel();
        lblResultShootJ2 = new javax.swing.JLabel();
        lblResultTotalPointJ2 = new javax.swing.JLabel();
        lblBalon2J1 = new javax.swing.JPanel();
        rbTurno3Player1 = new javax.swing.JRadioButton();
        rbTurno1Player1 = new javax.swing.JRadioButton();
        rbTurno2Player1 = new javax.swing.JRadioButton();
        barJugador1 = new javax.swing.JProgressBar();
        lblBalon1J1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblBalon3J1 = new javax.swing.JLabel();
        lblBalon4J1 = new javax.swing.JLabel();
        lblBalon5J1 = new javax.swing.JLabel();
        btnLanzarJugador1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        lblResultdadoLanzarJ1 = new javax.swing.JLabel();
        lblTotalPontsJ1 = new javax.swing.JLabel();
        lblResultShootJ1 = new javax.swing.JLabel();
        lblResultTotalPointJ1 = new javax.swing.JLabel();
        btnPlayAgain = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlPlayer2.setBorder(javax.swing.BorderFactory.createTitledBorder("Jugador 2"));

        playerTwoGroup.add(rbTurno3Player2);
        rbTurno3Player2.setText("Turno Tres");

        playerTwoGroup.add(rbTurno1Player2);
        rbTurno1Player2.setText("Turno Uno");

        playerTwoGroup.add(rbTurno2Player2);
        rbTurno2Player2.setText("Turno Dos");

        barJugador2.setMaximum(50);
        barJugador2.setValue(10);

        lblBalon1J2.setText("Balón 1");

        lblBalon2J2.setText("Balón 2");

        lblBalon3J2.setText("Balón 3");

        lblBalon4J2.setText("Balón 4");

        lblBalon5J2.setText("Balón 5");

        btnLanzarJugador2.setText("Lanzar");
        btnLanzarJugador2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLanzarJugador2ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultado"));

        lblResultdadoLanzarJ2.setText("Resultado de lanzamiento:");

        lblTotalPontsJ2.setText("Total de puntos:");

        lblResultShootJ2.setText("jLabel8");

        lblResultTotalPointJ2.setText("jLabel8");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblResultdadoLanzarJ2)
                    .addComponent(lblTotalPontsJ2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblResultTotalPointJ2)
                    .addComponent(lblResultShootJ2))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblResultdadoLanzarJ2)
                    .addComponent(lblResultShootJ2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalPontsJ2)
                    .addComponent(lblResultTotalPointJ2))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlPlayer2Layout = new javax.swing.GroupLayout(pnlPlayer2);
        pnlPlayer2.setLayout(pnlPlayer2Layout);
        pnlPlayer2Layout.setHorizontalGroup(
            pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPlayer2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlPlayer2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblBalon1J2)
                        .addGap(18, 18, 18)
                        .addComponent(lblBalon2J2)
                        .addGap(18, 18, 18)
                        .addComponent(lblBalon3J2)
                        .addGap(15, 15, 15)
                        .addComponent(lblBalon4J2)
                        .addGap(18, 18, 18)
                        .addComponent(lblBalon5J2))
                    .addComponent(barJugador2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPlayer2Layout.createSequentialGroup()
                        .addComponent(rbTurno1Player2)
                        .addGap(18, 18, 18)
                        .addComponent(rbTurno2Player2)
                        .addGap(18, 18, 18)
                        .addComponent(rbTurno3Player2)))
                .addGroup(pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPlayer2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPlayer2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLanzarJugador2)
                        .addGap(88, 88, 88))))
        );
        pnlPlayer2Layout.setVerticalGroup(
            pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPlayer2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(barJugador2, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLanzarJugador2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBalon1J2)
                    .addComponent(lblBalon2J2)
                    .addComponent(lblBalon3J2)
                    .addComponent(lblBalon4J2)
                    .addComponent(lblBalon5J2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPlayer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rbTurno1Player2)
                        .addComponent(rbTurno2Player2)
                        .addComponent(rbTurno3Player2)))
                .addGap(48, 48, 48))
        );

        lblBalon2J1.setBorder(javax.swing.BorderFactory.createTitledBorder("Jugador 1"));

        playerOneGroup.add(rbTurno3Player1);
        rbTurno3Player1.setText("Turno Tres");

        playerOneGroup.add(rbTurno1Player1);
        rbTurno1Player1.setText("Turno Uno");

        playerOneGroup.add(rbTurno2Player1);
        rbTurno2Player1.setText("Turno Dos");

        barJugador1.setMaximum(50);
        barJugador1.setValue(10);

        lblBalon1J1.setText("Balón 1");

        jLabel11.setText("Balón 2");

        lblBalon3J1.setText("Balón 3");

        lblBalon4J1.setText("Balón 4");

        lblBalon5J1.setText("Balón 5");

        btnLanzarJugador1.setText("Lanzar");
        btnLanzarJugador1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLanzarJugador1ActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultado"));

        lblResultdadoLanzarJ1.setText("Resultado de lanzamiento:");

        lblTotalPontsJ1.setText("Total de puntos:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblResultdadoLanzarJ1)
                    .addComponent(lblTotalPontsJ1))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblResultTotalPointJ1)
                    .addComponent(lblResultShootJ1))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblResultdadoLanzarJ1)
                    .addComponent(lblResultShootJ1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalPontsJ1)
                    .addComponent(lblResultTotalPointJ1))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout lblBalon2J1Layout = new javax.swing.GroupLayout(lblBalon2J1);
        lblBalon2J1.setLayout(lblBalon2J1Layout);
        lblBalon2J1Layout.setHorizontalGroup(
            lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lblBalon2J1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(lblBalon2J1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblBalon1J1)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(lblBalon3J1)
                        .addGap(15, 15, 15)
                        .addComponent(lblBalon4J1)
                        .addGap(18, 18, 18)
                        .addComponent(lblBalon5J1))
                    .addComponent(barJugador1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lblBalon2J1Layout.createSequentialGroup()
                        .addComponent(rbTurno1Player1)
                        .addGap(18, 18, 18)
                        .addComponent(rbTurno2Player1)
                        .addGap(18, 18, 18)
                        .addComponent(rbTurno3Player1)))
                .addGroup(lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(lblBalon2J1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lblBalon2J1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLanzarJugador1)
                        .addGap(88, 88, 88))))
        );
        lblBalon2J1Layout.setVerticalGroup(
            lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lblBalon2J1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(barJugador1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLanzarJugador1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBalon1J1)
                    .addComponent(jLabel11)
                    .addComponent(lblBalon3J1)
                    .addComponent(lblBalon4J1)
                    .addComponent(lblBalon5J1))
                .addGroup(lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(lblBalon2J1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(58, 58, 58))
                    .addGroup(lblBalon2J1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(lblBalon2J1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbTurno1Player1)
                            .addComponent(rbTurno2Player1)
                            .addComponent(rbTurno3Player1))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        btnPlayAgain.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnPlayAgain.setText("Volver a jugar");
        btnPlayAgain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayAgainActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblBalon2J1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnlPlayer2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(197, 197, 197)
                        .addComponent(btnPlayAgain, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblBalon2J1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(btnPlayAgain, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(pnlPlayer2, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLanzarJugador1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLanzarJugador1ActionPerformed
        //ViewData = Jugador, Balon{1,2,3,4,5}], Estante, ResultadoDeLanzamiento, TotalPuntos, finalizado
        int[] valuesReceived1 = new int[6];
        updatedValues1[0] = 1;
        
        valuesReceived1 = logic.playProcess(updatedValues1);// TODO add your handling code here:
        updatedValues1 = valuesReceived1;
        
        //cambia la barra de estado
        if(updatedValues1[1]-1 == 1){
            barJugador1.setValue(10);
        }else if(updatedValues1[1]-1 == 2){
            barJugador1.setValue(20);
        }else if(updatedValues1[1]-1 == 3){
            barJugador1.setValue(30);
        }else if(updatedValues1[1]-1 == 4){
            barJugador1.setValue(40);
        }else{
            barJugador1.setValue(50);
        }
        
        //cambia los radioButtons
        if(updatedValues1[1] == 5 && updatedValues1[2] == 1){
            rbTurno1Player1.doClick();
        }else if(updatedValues1[1] == 5 && updatedValues1[2] == 2){
            rbTurno2Player1.doClick();
        }else{
            rbTurno3Player1.doClick();
        }
        
        //cambia resultados del lanzamientos
        if(updatedValues1[3] == 0){
            lblResultShootJ1.setText("Fallido");
        }else{
            lblResultShootJ1.setText("Exitoso");
        }
        
        //cambia los resultados totales
        lblResultTotalPointJ1.setText(Integer.toString(updatedValues1[4]));
        
        if (updatedValues1[5] == 1){
            playPlayer1 = false;
            //deshabilito bot[on
        }
        
        //hago visible a playerAgain
        if (playPlayer2 == playPlayer1) {
            startAgain = true;
        }
    }//GEN-LAST:event_btnLanzarJugador1ActionPerformed

    private void btnLanzarJugador2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLanzarJugador2ActionPerformed
        // TODO add your handling code here:
        
        //Al final del codigo
        lblResultTotalPointJ2.setText(Integer.toString(updatedValues2[4]));
        
        if (updatedValues2[5] == 1){
            playPlayer2 = false;
            //deshabilito bot[on
        }
        
        //hago visible a playerAgain
        if (playPlayer2 == playPlayer1) {
            startAgain = true;
        }
    }//GEN-LAST:event_btnLanzarJugador2ActionPerformed

    private void btnPlayAgainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayAgainActionPerformed
        // TODO add your handling code here:
     
    }//GEN-LAST:event_btnPlayAgainActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barJugador1;
    private javax.swing.JProgressBar barJugador2;
    private javax.swing.JButton btnLanzarJugador1;
    private javax.swing.JButton btnLanzarJugador2;
    private javax.swing.JButton btnPlayAgain;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblBalon1J1;
    private javax.swing.JLabel lblBalon1J2;
    private javax.swing.JPanel lblBalon2J1;
    private javax.swing.JLabel lblBalon2J2;
    private javax.swing.JLabel lblBalon3J1;
    private javax.swing.JLabel lblBalon3J2;
    private javax.swing.JLabel lblBalon4J1;
    private javax.swing.JLabel lblBalon4J2;
    private javax.swing.JLabel lblBalon5J1;
    private javax.swing.JLabel lblBalon5J2;
    private javax.swing.JLabel lblResultShootJ1;
    private javax.swing.JLabel lblResultShootJ2;
    private javax.swing.JLabel lblResultTotalPointJ1;
    private javax.swing.JLabel lblResultTotalPointJ2;
    private javax.swing.JLabel lblResultdadoLanzarJ1;
    private javax.swing.JLabel lblResultdadoLanzarJ2;
    private javax.swing.JLabel lblTotalPontsJ1;
    private javax.swing.JLabel lblTotalPontsJ2;
    private javax.swing.ButtonGroup playerOneGroup;
    private javax.swing.ButtonGroup playerTwoGroup;
    private javax.swing.JPanel pnlPlayer2;
    private javax.swing.JRadioButton rbTurno1Player1;
    private javax.swing.JRadioButton rbTurno1Player2;
    private javax.swing.JRadioButton rbTurno2Player1;
    private javax.swing.JRadioButton rbTurno2Player2;
    private javax.swing.JRadioButton rbTurno3Player1;
    private javax.swing.JRadioButton rbTurno3Player2;
    // End of variables declaration//GEN-END:variables

}
